## Installation
``` git clone https://gitlab.com/candyblue/starter-kirby.git ```

## Make Updates
Making updates is as easy as hell, kirby is added as git-submodule and can be updated with ``` ./update.sh ```, executed in root.
