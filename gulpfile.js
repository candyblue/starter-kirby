'use strict';

const { src, dest, parallel, series, watch }   = require('gulp');
const sass                      = require('gulp-sass');
sass.compiler                   = require('node-sass');
//const minifyCSS                 = require('gulp-csso');
//const concat                    = require('gulp-concat');

const paths = {
  input: {
    assets: './src/assets/**/*',
    scss: './src/assets/scss/**/*.+(scss|sass)',
    js: './src/assets/js/**/*.js',
    img: './src/assets/img/**/*.jpg',
    svg: './src/assets/svg/**/*.svg',
    fonts: './src/assets/fonts',
  },
  output: {
    dir: './dist',
    content: './dist/content',
    site: './dist/site',
    kirby: './dist/kirby',
    assets: './src/assets/**/*',
    css: './src/assets/css/',
    js: './src/assets/js',
    img: './src/assets/img',
    svg: './src/assets/img',
    fonts: './src/assets/fonts'
  }
};


function css() {
  return src(paths.input.scss)
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(dest(paths.output.css))
}

/*
function js() {
  return src('client/javascript/*.js', { sourcemaps: true })
    .pipe(concat('app.min.js'))
    .pipe(dest('build/js', { sourcemaps: true }))
}

exports.js = js;
*/
exports.css = css;
exports.watch = watch;

exports.default = function() {
  // You can use a single task
  watch(paths.input.scss, { ignoreInitial: false }, css);
  // Or a composed task
  //watch('src/*.js', series(clean, javascript));
};
